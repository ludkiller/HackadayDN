#!/usr/bin/env node

var debug = require('debug')('HackadayDN');
var log = require('./utils/log.js');
var app = require('./app.js');
//hook console.log,.info,.warn,.error,.success

app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), function() {
    debug('Express server listening on port ' + server.address().port);
});
