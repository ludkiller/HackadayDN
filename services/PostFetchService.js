(function(){

	'use_strict';
	//use --use_strict param 
	/**
	 * PostFetch Service for real time updation of Server DB.
	 * 
	 */


	var xml2js = require('xml2js');
	var mongoose = require('mongoose');
	var moment = require('moment');
	var PostSchema = require('../model/PostSchema.js');

	var request = require('request');
	var FEED_URL_PAGED  = "https://hackaday.com/blog/feed/?paged=";

	//10 mins 10 * 60 * 1000
	var POLL_INTERVAL = 600000; 

	//Maths
	//Every request transfers a Max of 100KB
	//After every 10 mins a Request is initiated
	//So 6 in 1 hour gives us 24 x 6 = 144 in a day
	//144 x 100 KB in a day ~ 144000/1024 = 14.0625 MB in a day.
	//that gives us a close approximate of _5,132.8125_ MB in a year
	//which is roughly equal to 5.13 GB 
	//100KB is the hard limit usually it is 60KB or 40KB.

	mongoose.connect('mongodb://localhost/hackadayDN');

	var db = mongoose.connection;
	db.on('error', function(err){
		console.log('connection error: ' + err)
	});



	db.once('open', function (callback) {

			/**
			 * Do request.
			 */


			var Count404 = 0;

			var pageId = 1;

			var IMAGE_URL = "http://1.gravatar.com/blavatar/5560f98f805877b0e332f191cb9e0af3?s=96&#038;d=http%3A%2F%2Fs2.wp.com%2Fi%2Fbuttonw-com.png"

			var BlogModel = mongoose.model('PostEntry',PostSchema);


			var responseHandler = function (error, response, body) {
				console.log("Updating page : " + pageId);
				//No need
				//pageId++;
				if (!error && response.statusCode == 200) {
					//console.log(body);
					
					xml2js.parseString(body,function(err,result){

						if(err)	{
							console.log(err);
							request(FEED_URL_PAGED + pageId, responseHandler);
							return;
						}
						
						if(typeof result.rss === 'undefined')	{
							console.log("RSS Undefined");
							request(FEED_URL_PAGED + pageId, responseHandler);
							return;
						}

						var items = result.rss.channel[0].item;
						let onFrontPageSet = new Set();

						BlogModel.find({
							isOnFrontPage : true
						}).exec(function(err,_blogEntries){
							for(let entry of _blogEntries)	{
								onFrontPageSet.add(entry);
							}
							let postCount = 0;

							if(typeof items !== 'undefined')	{
								for(let item of items)	{

									if(typeof item !== 'undefined')	{
										//Do stuff with each items
										let title = item['title'][0];
										let link = item['link'][0];
										let comments = item['comments'][0];
										let pubDate = item['pubDate'][0];
										let pubTimeStamp = moment(new Date(pubDate.split(", ")[1].split("+")[0])).valueOf();
										let creator = item['dc:creator'][0];
										let categories = [];
										for(let category of item['category'])	{
											categories.push(category);
										}
										let guid = typeof item['guid'][0]['_'].split("=")[1] !== 'undefined' ? item['guid'][0]['_'].split("=")[1] : item['guid'][0]['_'];
										let guidLink = item['guid'][0]['_'];
										let description = item['description'][0];
										let content = item['content:encoded'][0];
										let commentRssFeed = item['wfw:commentRss'][0];
										let mediaThumbnail = typeof item['media:thumbnail'] !== 'undefined' ? item['media:thumbnail'][0]['$']['url'] : IMAGE_URL ;
										let mediaContent = [];
										for(let media of item['media:content'])	{
											
											let mediaData = {
												'url' : typeof media['$']['url'] !== 'undefined' ? media['$']['url'] : '',
												'title' : typeof media['media:title'] !== 'undefined' ? media['media:title'][0]['_'] : '',

											};
											mediaContent.push(mediaData);

										}

										

										BlogModel.findOne({
											guid : guid
										}).exec(function(err,_blogEntry){
											let blogEntry;
											//Check if it already exists if it doesn't add it.
											//
											if(err)	{
												console.log(err);
											}
											blogEntry = new BlogModel({ 
												guid : guid,
												title : title,
												link : link,
												comments : comments,
												pubDate : pubDate,
												pubTimeStamp : pubTimeStamp,
												creator : creator,
												categories : categories,
												guidLink : guidLink,
												description : description,
												content : content,
												commentRssFeed : commentRssFeed,
												mediaThumbnail : mediaThumbnail,
												mediaContent : mediaContent,
												isOnFrontPage : true
											});
											//_blogEntry != null ? console.log(_blogEntry['guid']) : console.log('Not in cache.');
											postCount++;
											if(typeof guid !== 'undefined' && _blogEntry == null)	{
												blogEntry.save(function(err,posts){
													if(err) return console.log(err);
													console.log("Added Post with GUID : " + guid);					
												});
											}
											else {
												if(_blogEntry['guid'] == guid)	{
													onFrontPageSet.forEach(function(__entry){
														if(guid == __entry['guid'])	{
															// console.log("Patching GUID : " + guid);
															onFrontPageSet.delete(__entry);
														}
													});
													
													console.log('Failed, Possible duplicate : ' + guid);
												}
												else {
													console.log('Failed, Possible duplicate (onFrontPage) : '  + guid);
												}


												
											}
											if(postCount == 7)	{
												console.log('Performing Clean up ...');
												for(let entry of onFrontPageSet)	{

													BlogModel.findOneAndUpdate({
														guid : entry['guid']
													},
													{
														isOnFrontPage : false
													}).exec(function(err,_entry)	{
														console.log("Post with GUID is no more on Front Page : " + _entry.guid);
														// _entry.update(function(err,posts){
														// 	if(err) return console.log(err);
															
														// });
													});
												}
												onFrontPageSet.clear();
											}

										});
									}
									
								}
								//set onFrontPageSet remaining items to false.


							}

						});
							
					});
				}

				
				
			};
			console.log('Init Service');
			request(FEED_URL_PAGED + pageId, responseHandler);
			setInterval(request, POLL_INTERVAL, FEED_URL_PAGED + pageId, responseHandler);



	});

}());
