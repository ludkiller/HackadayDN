var mongoose = require('mongoose');



/**
 * MediaContent Description.
 * @typedef {Object} MediaContent
 * @property {String} url - Url to media content.
 * @property {String} title - Title for media content(if any).
 */
/**
 * PostSchema Format
 * @type {Object}
 * @property {String} guid - a Globally Unique Identifier.
 * @property {String} title - Title of the Post.
 * @property {String} link - Link to the Post.
 * @property {String} link - Link to the comment page.
 * @property {String} pubDate - Published Date.
 * @property {String} creator - Creator name.
 * @property {String[]} categories - Categories string array.
 * @property {String} guidLink - Guid Link.
 * @property {String} description - Short description of post.
 * @property {String} content - Content of post.
 * @property {String} commentRssFeed - RSS feed to comments.
 * @property {String} mediaThumbnail - Post favicon thingy.
 * @property {MediaContent} mediaContent - Alternate images on a post.
 * @property {Boolean} isOnFrontPage : True if posts is on Front Page. 
 * 
 */
var PostSchema = mongoose.Schema({
	guid : String,
	title : String,
	link : String,
	comments : String,
	pubDate : String,
	pubTimeStamp : {type : Date},
	creator : String,
	categories : [String],
	guidLink : String,
	description : String,
	content : String,
	commentRssFeed : String,
	mediaThumbnail : String,
	mediaContent : [
		{
			url: String, 
			title: String
		}
	],
	isOnFrontPage : Boolean

});

//Use this in Production
//PostSchema.set('autoIndex', false);

PostSchema.statics.getFrontPagePosts = function(cb)	{
	return this.model('PostEntry').find({ isOnFrontPage : true })
	.sort({ pubTimeStamp : -1})
	.exec(cb);
}

PostSchema.statics.getPosts = function(start,limit,cb)	{
	//do fancy stuff here
	return this.model('PostEntry').find()
	.sort({ pubTimeStamp : -1})
	.skip(start)
	.limit(limit)
	.exec(cb);
}

module.exports = PostSchema;