var documentation = require('gulp-documentation'),
    gulp = require('gulp');



gulp.task('default', function() {
  // place code for your default task here
});


gulp.task('documentation', function () {

//  gulp.src('./routes/index.js')
//    .pipe(documentation({ format: 'md' }))
//    .pipe(gulp.dest('md-documentation'));

  //gulp.src(['./model/PostSchema.js','./routes/index.js'])
  gulp.src(['./model/PostSchema.js','./routes/*.js'])
    .pipe(documentation({ format: 'html' }))
    .pipe(gulp.dest('public'));

//  gulp.src('./index.js')
//    .pipe(documentation({ format: 'json' }))
//    .pipe(gulp.dest('json-documentation'));

});
