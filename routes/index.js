﻿var express = require('express');
var router = express.Router();
//var log = require('../utils/log.js');
var mongoose = require('mongoose');
var moment = require('moment');

mongoose.connect('mongodb://localhost/hackadayDN');

var PostSchema = require('../model/PostSchema.js');
var BlogModel = mongoose.model('PostEntry',PostSchema);


var db = mongoose.connection;
db.on('error', function(err){
	console.error('connection error: ' + err);
	if(err.message.split(" ")[1] === 'ECONNREFUSED')	{
		console.info("Make sure MongoDB is up and running.");
	}
	//write a wrapper around different process signals
	//make sure we clean up before leaving.
	process.exit(0);
});

db.once('open',function(){
	//Once db is open do stuff.
	console.success('connected to mongoDB');
});


router.get('/', function (req, res) {
    res.render('index', { title: 'HackAnotherDayDN' });
});


/**
 * Fetch Front Page posts from DB. For now Post with isOnFrontPage set to true are returned in reverse chronological order.
 * see {@link #PostSchema}
 * @name getFrontPagePosts
 * @namespace HDN
 * @return {JSON} FrontPagePosts - Returns JSON array of Posts to be displayed on Dashboard.
 *
 * @example 
 * //Returns Post which has isOnFrontPage set to true.
 * /getFrontPagePosts
 * http://hdn.futuretraxex.com/getFrontPagePosts
 *
 */
router.get('/getFrontPagePosts', function (req,res){
	BlogModel.getFrontPagePosts(function(err,entries){
		res.json(entries);
	});
});

/**
 * Fetch Posts from underlying DB in reverse chronological order.
 * see {@link #PostSchema}
 * @name getPosts
 * 
 * @param {Integer} [page=1] what page to fetch.
 * @param {Integer} [limit=7] number of items to fetch at a time.
 * @return {JSON} Posts - Returns JSON array of requested Posts.
 *
 * @example 
 * //Returns Post 2.
 * getPosts?page=2&limit=1
 * http://hdn.futuretraxex.com/getPosts?page=2&limit=1
 */
router.get('/getPosts', function(req,res){
	req.query.page = typeof req.query.page !== 'undefined' ? req.query.page : 1;
	req.query.limit = typeof req.query.limit !== 'undefined' ? req.query.limit : 7;
	var start = (req.query.page - 1) * req.query.limit;
	BlogModel.getPosts(start,req.query.limit,function(err,entries){
		res.json(entries);
	});

});

console.success('Registered routes');

module.exports = router;