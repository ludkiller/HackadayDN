//NOTE : Deprecated and probably doesn't work don't use this 
//why is it here then you ask ? 
//Historical reasons.

//Doesn't work rather use --use_strict while running node :(
"use_strict";
/// <reference path="typings/tsd.d.ts" />
/**
 * HackadayDN Initial bootstrap script
 * 
 * To be run before deploying the server.
 * 
 * Fetches Hackaday Blog entries and stores them in a local DB.
 * As of now we are using a redis DB, might think of swithcing to MongoDB.
 * 
 */

var xml2js = require('xml2js');
var redis = require('redis');

//var client = redis.createClient();

var request = require('request');
var FEED_URL_PAGED  = "https://hackaday.com/blog/feed/?paged=";
//var FEED_URL_PAGED_1 = "https://hackaday.com/blog/feed/?paged=1";

(function(){

	/**
	 * Do request.
	 */

	
	var Count404 = 0;

	var pageId = 1;

	var IMAGE_URL = "http://1.gravatar.com/blavatar/5560f98f805877b0e332f191cb9e0af3?s=96&#038;d=http%3A%2F%2Fs2.wp.com%2Fi%2Fbuttonw-com.png"



	var responseHandler = function (error, response, body) {
		console.log("Fetching page : " + pageId);
		pageId++;
		if (!error && response.statusCode == 200) {
			//console.log(body);
			xml2js.parseString(body,function(err,result){
				var items = result.rss.channel[0].item;
				if(typeof items !== 'undefined')	{

					for(let item of items)	{
						if(typeof item !== 'undefined')	{
							let client = redis.createClient();
							client.on("error", function (err) {
							    console.log("Error " + err);
							});
							//console.log(item['media:thumbnail']);
							//console.log(items[item]);
							//Do stuff with each items
							let title = item['title'][0];
							let link = item['link'][0];
							let comments = item['comments'][0];
							let pubDate = item['pubDate'][0];
							let creator = item['dc:creator'][0];
							let categories = [];
							for(let category of item['category'])	{
								categories.push(category);
							}
							let guid = typeof item['guid'][0]['_'].split("=")[1] !== 'undefined' ? item['guid'][0]['_'].split("=")[1] : item['guid'][0]['_'];
							let guidLink = item['guid'][0]['_'];
							let description = item['description'][0];
							let content = item['content:encoded'][0];
							let commentRssFeed = item['wfw:commentRss'][0];
							let mediaThumbnail = typeof item['media:thumbnail'] !== 'undefined' ? item['media:thumbnail'][0]['$']['url'] : IMAGE_URL ;
							let mediaContent = [];
							for(let media of item['media:content'])	{
								
								let mediaData = {
									'media:url' : typeof media['$']['url'] !== 'undefined' ? media['$']['url'] : '',
									'media:title' : typeof media['media:title'] !== 'undefined' ? media['media:title'][0]['_'] : '',

								};
								mediaContent.push(mediaData);

							}

							if(typeof guid !== 'undefined')	{
								client.hset(guid,"title",title);
								client.hset(guid,"link",link);
								client.hset(guid,"comments",comments);
								client.hset(guid,"pubDate",pubDate);
								client.hset(guid,"creator",creator);
								client.hset(guid,"categories",categories);
								client.hset(guid,"guidLink",guidLink);
								client.hset(guid,"description",description);
								client.hset(guid,"content",content);
								client.hset(guid,"commentRssFeed",commentRssFeed);
								client.hset(guid,"media:thumbnail",mediaThumbnail);
								client.hset(guid,"media:content",mediaContent);

								client.quit();
								console.log("Added Post with GUID : " + guid);
							}
							else {
								console.log("Failed");
							}



							
							
						
						}
						
						
					}
				}
				
			});
			
			/**
			 * Add setTimeout here if the requests are too fast.
			 */
			
			request(FEED_URL_PAGED + pageId, responseHandler);
		}
		else if (response.statusCode == 404)	{
			Count404++;
			//If 5 consecutive pages 404'd stop bootstrapping.
			if(Count404 > 5)	{
				console.log("[Error] Stopping 4 Page 404'd");
				return;
			}
			request(FEED_URL_PAGED + pageId, responseHandler);
		}
		else {
			Count404++;
			if(Count404 > 5)	{
				console.log("[Error] Stopping 4 Page 404'd");
				return;
			}
			request(FEED_URL_PAGED + pageId, responseHandler);
		}
	}

	request(FEED_URL_PAGED + pageId, responseHandler);

}());


