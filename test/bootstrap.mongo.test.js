var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/hackadayDN');

var PostSchema = require('../model/PostSchema.js');
var BlogModel = mongoose.model('PostEntry',PostSchema);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
	BlogModel.find({
		categories : '/Featured/i'
	})
	.sort({ pubTimeStamp : -1})
	.exec(function(err,data){
		console.log(data);
	});
});
